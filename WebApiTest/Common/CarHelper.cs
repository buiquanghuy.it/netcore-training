﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest.Models;
using WebApiTest.Services.Dtos;

namespace WebApiTest.Common
{
    public class CarHelper
    {
        public static List<CarDto> Cars = new List<CarDto>();
        public static void GetMockCar()
        {
            using (StreamReader sr = new StreamReader("Data/cars.json"))
            {
                // Read the stream to a string, and write the string to the console.
                var carText = sr.ReadToEnd();
                Cars = JsonConvert.DeserializeObject<List<CarDto>>(carText);
            }
        }
    }
}
