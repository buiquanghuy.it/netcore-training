﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiTest.Models;

namespace WebApiTest.Common
{
    public class TokenGenerator
    {
        public static JWTModel Generate()
        {
            return new JWTModel
            {
                AccessToken = StringHelper.RandomString(50),
                RefreshToken = StringHelper.RandomString(50),
                ExpireIn = DateTime.Now.AddDays(1)
            };
        }
    }
}
