﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiTest.Core.Domain;
using WebApiTest.Services.Dtos;

namespace WebApiTest.Services.Mapper
{
    public static class CarMapper
    {
        public static CarDto FromCarEntity(this CarEntity entity)
        {
            return new CarDto
            {
                Id = entity.Id,
                Name = entity.Name,
                Model = entity.Model,
                ModelYear = entity.ModelYear
            };
        }
    }
}
