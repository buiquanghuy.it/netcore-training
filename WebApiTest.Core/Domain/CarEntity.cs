﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTest.Core.Domain
{
    public class CarEntity : BaseEntity
    {
        public string Name { get; set; }
        public string Model { get; set; }
        public int ModelYear { get; set; }
    }
}
